## Project Description
William Hills Tech Challenge

## Dev Environment
Visual Studio 2015

## Dev Language
C#

## How to run?
TechChallenge.Console is a console app can be compiled and run directly.

## Project Structure
- TechChallenge.Core:
Contains the main business logic.
- TechChallenge.Tests:
Contains all unit tests.
- TechChallenge.Console:
A UI console to display task results.

## Dependencies
- Autofac
- AutoMapper
- CsvHelper
- nunit
- moq

## Implementation
- IBetRepository: reading datasource from csvs.
- IRiskService: the business logic goes into the class. please refer to RiskServiceTests for test cases coverage.

## Known issues
- Cache might need in BetRepository to avoid loading from files for every read.
- Some constants can be abstracted from RiskService to make them configurable in AppSettings.
- ViewModels and their mappings can be different according to what need to see in the UI.

## Author
Jing Wu (wjshome@gmail.com)
13/6/2016