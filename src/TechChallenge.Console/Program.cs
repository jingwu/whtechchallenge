﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using TechChallenge.Console.ViewModels;
using TechChallenge.Core.DataEntities;
using TechChallenge.Core.Repositories;
using TechChallenge.Core.Services;
using static System.Console;

namespace TechChallenge.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            // setup IoC: here manually register components, while the other way is to do automatically components and module scan,
            // as all dependencies are implemented IDependency interface
            var builder = new ContainerBuilder();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterType<BetRepository>().As<IBetRepository>();
            builder.RegisterType<RiskService>().As<IRiskService>();

            // init AutoMapper as we should use view model for all displaying
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<BetEntity, SettleBetViewModel>()
                    .ForMember(dst => dst.Win, opt => opt.MapFrom(src => src.WinOrToWin));
                cfg.CreateMap<BetEntity, UnsettleBetViewModel>()
                    .ForMember(dst => dst.ToWin, opt => opt.MapFrom(src => src.WinOrToWin));
            });

            // display
            using (var container = builder.Build())
            {
                var riskyService = container.Resolve<IRiskService>();
                WriteLine("Task 1");
                WriteLine("1.1 Settled bet history for a customer that shows they are winning at an unusual rate:");
                PrintSettleBetViewModels(Mapper.Map<IEnumerable<SettleBetViewModel>>(await riskyService.GetSettledRiskyBets()));
                WriteLine("------------------------------");

                WriteLine("Task 2");
                WriteLine("2.1 All upcoming bets from customers that win at an unusual rate should be highlighted as risky:");
                PrintUnsettleBetViewModels(Mapper.Map<IEnumerable<UnsettleBetViewModel>>(await riskyService.GetUnsettledRiskyBets()));
                WriteLine("------------------------------");

                WriteLine("2.2 Bets where the stake is more than 10 times higher than that customer’s average bet in their betting history should be highlighted as unusual:");
                PrintUnsettleBetViewModels(Mapper.Map<IEnumerable<UnsettleBetViewModel>>(await riskyService.GetUnsettledUnusualBets()));
                WriteLine("------------------------------");

                WriteLine("2.3 Bets where the stake is more than 30 times higher than that customer’s average bet in their betting history should be highlighted as highly unusual:");
                PrintUnsettleBetViewModels(Mapper.Map<IEnumerable<UnsettleBetViewModel>>(await riskyService.GetUnsettledHighlyUnusualBets()));
                WriteLine("------------------------------");

                WriteLine("2.4 Bets where the amount to be won is $1000 or more:");
                PrintUnsettleBetViewModels(Mapper.Map<IEnumerable<UnsettleBetViewModel>>(await riskyService.GetUnsettledHighAmountToWinBets()));
                WriteLine("------------------------------");

                WriteLine("Author: Jing Wu");
            }
        }

        static void PrintSettleBetViewModels(IEnumerable<SettleBetViewModel> model)
        {
            foreach (var item in model)
            {
                WriteLine($"Customer {item.Customer}, Event {item.Event}, Participant {item.Participant}, Stake {item.Stake.ToString("C")}, Win {item.Win.ToString("C")}");
            }
        }

        static void PrintUnsettleBetViewModels(IEnumerable<UnsettleBetViewModel> model)
        {
            foreach (var item in model)
            {
                WriteLine($"Customer {item.Customer}, Event {item.Event}, Participant {item.Participant}, Stake {item.Stake.ToString("C")}, To Win {item.ToWin.ToString("C")}");
            }
        }
    }
}