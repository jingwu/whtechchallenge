﻿namespace TechChallenge.Console.ViewModels
{
    public class UnsettleBetViewModel
    {
        public int Customer { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
        public double Stake { get; set; }
        public double ToWin { get; set; }
    }
}