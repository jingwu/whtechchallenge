﻿namespace TechChallenge.Core.DataEntities
{
    public class BetEntity
    {
        public int Customer { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
        public double Stake { get; set; }
        public double WinOrToWin { get; set; }
    }
}