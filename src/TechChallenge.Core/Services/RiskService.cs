﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechChallenge.Core.DataEntities;
using TechChallenge.Core.Repositories;

namespace TechChallenge.Core.Services
{
    public class RiskService : IRiskService
    {
        private readonly IBetRepository _betRepository;

        private const double RiskyFactor = 1.6d; // A customer wins on more than 60% of their bets
        private const double UnusualFactor = 10d; // the stake is more than 10 times higher than that customer’s average bet in their betting history
        private const double HighlyUnusualFactor = 30d; // the stake is more than 30 times higher than that customer’s average bet in their betting history
        private const double HighAmountToWinFactor = 1000d; // the amount to be won is $1000 or more.

        public RiskService(IBetRepository betRepository)
        {
            _betRepository = betRepository;
        }

        public async Task<IList<BetEntity>> GetSettledRiskyBets()
        {
            var bets = await _betRepository.LoadSettledBets();
            var riskyCustomers = FilterRiskyCustomers(bets);
            var retVal = bets.Where(x => riskyCustomers.Contains(x.Customer));
            return retVal.ToList();
        }

        public async Task<IList<BetEntity>> GetUnsettledRiskyBets()
        {
            var bets = await _betRepository.LoadUnsettledBets();
            var riskyCustomers = FilterRiskyCustomers(bets);
            var retVal = bets.Where(x => riskyCustomers.Contains(x.Customer));
            return retVal.ToList();
        }

        private static IEnumerable<int> FilterRiskyCustomers(IEnumerable<BetEntity> bets)
        {
            var riskyCustomers = bets.GroupBy(x => x.Customer)
                .Select(x => new
                {
                    Customer = x.Key,
                    SumStake = x.Sum(y => y.Stake),
                    SumWin = x.Sum(y => y.WinOrToWin)
                })
                .Where(x => x.SumWin > x.SumStake*RiskyFactor)
                .Select(x => x.Customer);
            return riskyCustomers;
        }

        public async Task<IList<BetEntity>> GetUnsettledUnusualBets()
        {
            var averageBets = await CalculateAverageBets();
            var bets = await _betRepository.LoadUnsettledBets();
            var retVal = bets.Where(x => averageBets.ContainsKey(x.Customer) && x.Stake > averageBets[x.Customer]*UnusualFactor);
            return retVal.ToList();
        }

        public async Task<IList<BetEntity>> GetUnsettledHighlyUnusualBets()
        {
            var averageBets = await CalculateAverageBets();
            var bets = await _betRepository.LoadUnsettledBets();
            var retVal = bets.Where(x => averageBets.ContainsKey(x.Customer) && x.Stake > averageBets[x.Customer]*HighlyUnusualFactor);
            return retVal.ToList();
        }

        private async Task<Dictionary<int, double>> CalculateAverageBets()
        {
            var historicalBets = await _betRepository.LoadSettledBets();
            var averageBets = historicalBets.GroupBy(x => x.Customer)
                .Select(x => new
                {
                    Customer = x.Key,
                    AverageStake = x.Average(y => y.Stake)
                })
                .ToDictionary(x => x.Customer, x => x.AverageStake);
            return averageBets;
        }

        public async Task<IList<BetEntity>> GetUnsettledHighAmountToWinBets()
        {
            var bets = await _betRepository.LoadUnsettledBets();
            var retVal = bets.Where(x => x.WinOrToWin >= HighAmountToWinFactor);
            return retVal.ToList();
        }
    }
}