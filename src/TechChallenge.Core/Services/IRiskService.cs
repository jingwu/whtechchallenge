﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TechChallenge.Core.DataEntities;

namespace TechChallenge.Core.Services
{
    public interface IRiskService : IDependency
    {
        Task<IList<BetEntity>> GetSettledRiskyBets();

        Task<IList<BetEntity>> GetUnsettledRiskyBets();
        Task<IList<BetEntity>> GetUnsettledUnusualBets();
        Task<IList<BetEntity>> GetUnsettledHighlyUnusualBets();
        Task<IList<BetEntity>> GetUnsettledHighAmountToWinBets();
    }
}