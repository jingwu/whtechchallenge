﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Features.Indexed;
using CsvHelper;
using TechChallenge.Core.Data;
using TechChallenge.Core.DataEntities;

namespace TechChallenge.Core.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly IIndex<DataTypes, ICsvReader> _reader;

        public BetRepository(IIndex<DataTypes, ICsvReader> reader)
        {
            _reader = reader;
        }

        public Task<IList<BetEntity>> LoadSettledBets()
        {
            using (var reader = _reader[DataTypes.Settled])
            {
                var bets = reader.GetRecords<BetEntity>();
                var tcs = new TaskCompletionSource<IList<BetEntity>>();
                tcs.SetResult(bets.ToList());
                return tcs.Task;
            }
        }

        public Task<IList<BetEntity>> LoadUnsettledBets()
        {
            using (var reader = _reader[DataTypes.Unsettled])
            {
                var bets = reader.GetRecords<BetEntity>();
                var tcs = new TaskCompletionSource<IList<BetEntity>>();
                tcs.SetResult(bets.ToList());
                return tcs.Task;
            }
        }
    }
}