﻿using CsvHelper.Configuration;
using TechChallenge.Core.DataEntities;

namespace TechChallenge.Core.Repositories
{
    public sealed class BetMappers : CsvClassMap<BetEntity>
    {
        public BetMappers()
        {
            Map(m => m.Customer).Name("Customer");
            Map(m => m.Event).Name("Event");
            Map(m => m.Participant).Name("Participant");
            Map(m => m.Stake).Name("Stake");
            Map(m => m.WinOrToWin).Name("Win", "To Win");
        }
    }
}