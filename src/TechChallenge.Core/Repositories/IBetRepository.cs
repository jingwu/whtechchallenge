﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TechChallenge.Core.DataEntities;

namespace TechChallenge.Core.Repositories
{
    public interface IBetRepository : IDependency
    {
        Task<IList<BetEntity>> LoadSettledBets();
        Task<IList<BetEntity>> LoadUnsettledBets();
    }
}