﻿using System;
using System.IO;
using System.Linq;
using Autofac;
using CsvHelper;
using TechChallenge.Core.Data;

namespace TechChallenge.Core.Repositories
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CsvReader>()
                .Keyed<ICsvReader>(DataTypes.Settled)
                .OnPreparing(args => args.Parameters = args.Parameters.Concat(new[] {new NamedParameter("reader", new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\Settled.csv")))}))
                .OnActivating(args => args.Instance.Configuration.RegisterClassMap<BetMappers>());

            builder.RegisterType<CsvReader>()
                .Keyed<ICsvReader>(DataTypes.Unsettled)
                .OnPreparing(args => args.Parameters = args.Parameters.Concat(new[] {new NamedParameter("reader", new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\Unsettled.csv")))}))
                .OnActivating(args => args.Instance.Configuration.RegisterClassMap<BetMappers>());
        }
    }
}