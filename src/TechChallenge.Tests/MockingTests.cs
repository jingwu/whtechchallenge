﻿using Autofac.Extras.Moq;
using NUnit.Framework;
using TechChallenge.Core.Repositories;

namespace TechChallenge.Tests
{
    /// <summary>
    /// This is the base class using Autofac AutoMocking
    /// Usage REF: http://docs.autofac.org/en/latest/integration/moq.html
    /// </summary>
    public class MockingTests
    {
        /// <summary>
        /// AutoMock object offers mocking object abilities within its context
        /// </summary>
        protected AutoMock Mock;

        [OneTimeSetUp]
        public virtual void MockingTestFixtureSetUp()
        {
        }

        [OneTimeTearDown]
        public virtual void MockingTestFixtureTearDown()
        {
        }

        [SetUp]
        public virtual void MockingSetUp()
        {
            Mock = AutoMock.GetLoose();
            new RepositoryModule().Configure(Mock.Container.ComponentRegistry);
        }

        [TearDown]
        public virtual void MockingTearDown()
        {
            Mock.Dispose();
            Mock = null;
        }
    }
}