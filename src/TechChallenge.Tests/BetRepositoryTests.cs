﻿using System.Threading.Tasks;
using NUnit.Framework;
using TechChallenge.Core.Repositories;

namespace TechChallenge.Tests
{
    [TestFixture]
    public class BetRepositoryTests : MockingTests
    {
        [Test]
        public async Task LoadSettledBets_ShouldReturnListOfSettledBets()
        {
            // Arrange

            // Act
            var bets = await Mock.Create<BetRepository>().LoadSettledBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(50));

            var bet = bets[0]; // 1,1,6,50,250
            Assert.That(bet, Is.Not.Null);
            Assert.That(bet.Customer, Is.EqualTo(1));
            Assert.That(bet.Event, Is.EqualTo(1));
            Assert.That(bet.Participant, Is.EqualTo(6));
            Assert.That(bet.Stake, Is.EqualTo(50));
            Assert.That(bet.WinOrToWin, Is.EqualTo(250));
        }

        [Test]
        public async Task LoadUnsettledBets_ShouldReturnListOfUnsettledBets()
        {
            // Arrange

            // Act
            var bets = await Mock.Create<BetRepository>().LoadUnsettledBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(22));

            var bet = bets[0]; // 1,11,4,50,500
            Assert.That(bet, Is.Not.Null);
            Assert.That(bet.Customer, Is.EqualTo(1));
            Assert.That(bet.Event, Is.EqualTo(11));
            Assert.That(bet.Participant, Is.EqualTo(4));
            Assert.That(bet.Stake, Is.EqualTo(50));
            Assert.That(bet.WinOrToWin, Is.EqualTo(500));
        }
    }
}