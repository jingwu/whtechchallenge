﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TechChallenge.Core.DataEntities;
using TechChallenge.Core.Repositories;
using TechChallenge.Core.Services;

namespace TechChallenge.Tests
{
    [TestFixture]
    public class RiskServiceTests : MockingTests
    {
        [Test]
        public async Task GetSettledRiskyBets_ShouldReturnRiskyBets()
        {
            // Arranage
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 100, WinOrToWin = 161},
                    new BetEntity {Customer = 2, Stake = 100, WinOrToWin = 161}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetSettledRiskyBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task GetSettledRiskyBets_ShouldReturnRiskyBets_WhenMixedUnriskyCustomer()
        {
            // Arranage
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 100, WinOrToWin = 161},
                    new BetEntity {Customer = 2, Stake = 100, WinOrToWin = 161},
                    new BetEntity {Customer = 3, Stake = 100, WinOrToWin = 50}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetSettledRiskyBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(2)); // only customer 1's and customer 2's bets should return
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[1].Customer, Is.EqualTo(2));
        }

        [Test]
        public async Task GetSettledRiskyBets_ShouldReturnRiskyBets_WhenMixedMultipleBets()
        {
            // Arranage
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 100, WinOrToWin = 100},
                    new BetEntity {Customer = 1, Stake = 100, WinOrToWin = 160 + 160 + 1 - 100},
                    new BetEntity {Customer = 2, Stake = 100, WinOrToWin = 0},
                    new BetEntity {Customer = 3, Stake = 100, WinOrToWin = 50}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetSettledRiskyBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(2)); // only customer 1's bets should return
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[1].Customer, Is.EqualTo(1));
        }

        [Test]
        public async Task GetUnsettledRiskyBets_ShouldReturnRiskyBets()
        {
            // Arranage
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 100, WinOrToWin = 161},
                    new BetEntity {Customer = 2, Stake = 100, WinOrToWin = 161},
                    new BetEntity {Customer = 3, Stake = 100, WinOrToWin = 50}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledRiskyBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(2));
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[1].Customer, Is.EqualTo(2));
        }

        [Test]
        public async Task GetUnsettledUnusualBets_ShouldReturnUnusualBets()
        {
            // Arrange
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 50},
                    new BetEntity {Customer = 1, Stake = 150}
                }); // average stake is (50+150)/2=100

            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Event = 1, Stake = 50}, // less than 100*10
                    new BetEntity {Customer = 1, Event = 2, Stake = 1001} // more than 100*10
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledUnusualBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(1)); // only event 2 for customer should return
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[0].Event, Is.EqualTo(2));
        }

        [Test]
        public async Task GetUnsettledUnusualBets_ShouldReturnUnusualBets_WhenMixedCustomers()
        {
            // Arrange
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 50},
                    new BetEntity {Customer = 1, Stake = 150},
                    new BetEntity {Customer = 2, Stake = 100}
                }); // average stake for customer 1 is (50+150)/2=100, for customer 2 is 100

            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Event = 1, Stake = 50}, // c1: less than 100*10
                    new BetEntity {Customer = 1, Event = 2, Stake = 1001}, // c1: more than 100*10
                    new BetEntity {Customer = 2, Event = 3, Stake = 500} // c2: less than 100*10
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledUnusualBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(1)); // only event 2 for customer 1 should return
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[0].Event, Is.EqualTo(2));
        }

        [Test]
        public async Task GetUnsettledUnusualBets_ShouldReturnEmptyUnusualBets_WhenNoHistoryBets()
        {
            // Arrange
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>()); // no history

            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Event = 1, Stake = 100000}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledUnusualBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(0));
        }

        [Test]
        public async Task GetUnsettledHighlyUnusualBets_ShouldReturnUnusualBets()
        {
            // Arrange
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadSettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Stake = 50},
                    new BetEntity {Customer = 1, Stake = 150},
                    new BetEntity {Customer = 2, Stake = 100}
                }); // average stake for customer 1 is (50+150)/2=100, for customer 2 is 100

            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Event = 1, Stake = 50}, // c1: less than 100*30
                    new BetEntity {Customer = 1, Event = 2, Stake = 3001}, // c1: more than 100*30
                    new BetEntity {Customer = 2, Event = 3, Stake = 500} // c2: less than 100*30
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledHighlyUnusualBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(1)); // only event 2 for customer 1 should return
            Assert.That(bets[0].Customer, Is.EqualTo(1));
            Assert.That(bets[0].Event, Is.EqualTo(2));
        }

        [Test]
        public async Task GetUnsettledHighAmountToWinBets_ShouldReturnListOfBets()
        {
            // Arranage
            Mock.Mock<IBetRepository>()
                .Setup(x => x.LoadUnsettledBets())
                .ReturnsAsync(new List<BetEntity>
                {
                    new BetEntity {Customer = 1, Event = 1, WinOrToWin = 50},
                    new BetEntity {Customer = 1, Event = 2, WinOrToWin = 1000},
                    new BetEntity {Customer = 2, Event = 3, WinOrToWin = 1001}
                });

            // Act
            var bets = await Mock.Create<RiskService>().GetUnsettledHighAmountToWinBets();

            // Assert
            Assert.That(bets, Is.Not.Null);
            Assert.That(bets.Count, Is.EqualTo(2));
            Assert.That(bets[0].Event, Is.EqualTo(2));
            Assert.That(bets[1].Event, Is.EqualTo(3));
        }
    }
}